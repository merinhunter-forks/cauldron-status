---
title: Upgrade Cauldron to a new version
date: '2020-12-18T08:58:53.884Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-12-18T09:58:53.884Z'
---
We are upgrading cauldron with a patch to include some new features

<!--- language code: en -->
