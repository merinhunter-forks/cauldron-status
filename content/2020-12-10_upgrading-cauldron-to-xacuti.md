---
title: Upgrading Cauldron to Xacuti
date: '2020-12-10T11:29:04.532Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-12-10T11:45:24.226Z'
---
We are upgrading cauldron to a new version. It will take a few hours

<!--- language code: en -->
