---
title: 'Upgrade Cauldron to Orange release '
date: '2020-07-15T10:36:03.431Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-07-15T10:52:22.622Z'
---
We are updating Cauldron to a newer version. It will take a few hours.

<!--- language code: en -->
