---
title: Upgrade Cauldron to Peanut release
date: '2020-07-30T07:29:40.100Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-07-30T08:00:00.000Z'
---
We are updating Cauldron to a newer version. It will take a few minutes

<!--- language code: en -->
