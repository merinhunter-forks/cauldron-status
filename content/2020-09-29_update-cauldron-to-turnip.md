---
title: Update Cauldron to Turnip
date: '2020-09-29T07:23:51.984Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-10-13T14:38:59.246Z'
---
We are updating Cauldron to the latest version, it will take a few minutes

<!--- language code: en -->
