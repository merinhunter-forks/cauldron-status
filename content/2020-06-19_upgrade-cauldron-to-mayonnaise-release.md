---
title: 'Upgrade Cauldron to Mayonnaise release '
date: '2020-06-19T12:42:40.562Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-06-19T13:09:43.619Z'
---
We are updating Cauldron to a newer version. It will take a few hours.

<!--- language code: en -->
