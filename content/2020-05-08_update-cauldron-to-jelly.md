---
title: Update Cauldron to Jelly
date: '2020-05-08T08:00:00.000Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-05-08T08:30:00.000Z'
---
We are updating Cauldron to a newer version. It will take a few hours.

<!--- language code: en -->
