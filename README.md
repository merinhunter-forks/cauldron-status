# Cauldron Status Page

The Cauldron status page shows information about the status of the different services that make up Cauldron.

It is built on [Node.js](https://nodejs.org/) and powered by [Statusfy](https://statusfy.co/).

## Requirements

The requirements for this project are the same as Statusfy, which are described [here](https://docs.statusfy.co/guide/getting-started/).

## Install a development environment

To set up a development environment, just clone the repository into your local machine, go into and execute the following:

```sh
$ npm install
$ npm run dev
```

## Incidents

Statusfy uses Markdown to define incidents, which are stored as individual files in the `/content` directory of the repository.

The incidents can be cataloged according to their degree of severity:

* **under-maintenance**: The system(s) cannot handle requests due to a temporary maintenance update.
* **major-outage**: No one can access the system(s) because it is completely down.
* **partial-outage**: Limited access to the system(s), because it's probably experiencing difficulties.
* **degraded-performance**: The system(s) is not stable, it's working slow otherwise impacted in a minor way.

In addition, incidents may affect one or more of the following systems:

* **Web**
* **Kibana**
* **Elasticsearch**
* **Workers**

Here are described in a very simplified way some of the procedures to create and update incidents. For more detailed information, please visit the [official documentation](https://docs.statusfy.co/guide/incidents/).

### Create a new incident

You can directly add a Markdown incident file in the `/content` directory or use the npm commands that comes with the tool.

* **Directly creating an incident file**: You can submit an incident by creating a Markdown file, using the following template:

  ```md
  ---
  title: Title of the incident
  date: '2020-03-06T16:00:00.000Z'
  severity: under-maintenance
  affectedsystems:
    - web
    - kibana
    - elasticsearch
    - workers
  resolved: false
  ---
  Description of the incident

  <!--- language code: en -->
  ```

* **Creating from commands**: Statusfy allows you to create the incidents using some npm commands that comes with it. To create a new incident in this way, clone the repository and execute inside the directory:

  ```sh
  $ npm run new-incident
  ```

  An interactive form will prompt in the terminal. Just follow the guidelines and the incident will be created.

### Update an incident

Updating an incident can be done by manually modifying the incident's file or by using the npm tool provided by Statusfy.

* **Directly modifying the file**: By manually changing the `resolved` field of the file, you can mark the incident as resolved.

* **Updating from commands**: You can update the status of an incident by executing the following command on the project repository:

  ```sh
  $ npm run update-incident
  ```

### Delete an incident

You can delete an incident by removing its file from the `/content` directory or by executing the following command on the project repository:

  ```sh
  $ npm run delete-incident
  ```
